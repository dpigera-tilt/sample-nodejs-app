FROM node:8-alpine
RUN mkdir -p /usr/src/app
WORKDIR /usr/src/app
COPY sample-nodejs-app/package.json /usr/src/app/package.json
COPY sample-nodejs-app/app.js /usr/src/app/app.js
RUN cd /usr/src/app && cat package.json && npm install

EXPOSE 3000
CMD [ "node", "app.js" ]